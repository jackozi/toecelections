package Server.Controllers;

import Res.DataCoder;

import java.io.*;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 1/19/14
 * Time: 2:15 PM
 */
public class Connection extends Thread {
    private Server server;
    private BufferedReader in;
    private Socket socket;
    OutputStreamWriter out;

    /**
     * Constructor for the connection this class keeps
     * a connection open on the server side. Every time
     * the client requests data the server gets it in
     * here first. Then handles it by sending it to the
     * Server class. Also the server class sends data
     * here whenever it needs to be transported to the
     * client.
     * @param socket The socket that is open with the client
     * @param server The server that is running to handle requests
     */
    public Connection(Socket socket, Server server) {
        this.socket = socket;
        this.server = server;
        try {
            in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            out = new OutputStreamWriter(socket.getOutputStream());
            }   catch (IOException e) {
            System.out.print("\nSomething went wrong setting up the streams for sending/receiving data from a client.\n>> ");
        }
    }

    /**
     * The run class overridden from the Thread
     * superclass. Here this class keeps listening for input.
     */
    public void run() {
        String read;

        while (true) {
            try {
                read = in.readLine();
            } catch (IOException e) {
                read = null;
            }
            if (read != null) {
                if(read.equals("voteData")){
                    DataCoder.Polls.Poll.Builder rData = DataCoder.Polls.Poll.newBuilder();
                    try {
                        rData.mergeDelimitedFrom(socket.getInputStream());
                        FileOutputStream output = new FileOutputStream(rData.getId() + ".vd");
                        rData.build().writeTo(output);
                        server.readFile(rData.getId() + ".vd");
                        output.close();
                        socket.close();
                    } catch (IOException e) {
                        System.out.print("\nSomething went wrong receiving the poll data file from the client or writing it to disk.\n>> ");
                    }

                }else
                    server.executeCommand(read, this);
            }
        }
    }

    /**
     * Sends a data file to the client using writeDelimitedTo(socket.out)
     * @param p The data file to be sent. This must be DataCoder.Polls.Poll type of file.
     */
    public void sendFile(DataCoder.Polls.Poll p){
        try {
            p.writeDelimitedTo(socket.getOutputStream());
        } catch (IOException e) {
            System.out.print("\nUnable to send the data file containing the poll data. Requested for " + p.getId() + " at " + p.getLocation() + "\n>> ");
        }
    }
}
