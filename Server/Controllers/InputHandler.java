package Server.Controllers;

import Res.DataCoder;

import java.io.*;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 1/19/14
 * Time: 4:13 PM
 */
public class InputHandler extends Thread {
    private Server server;

    /**
     * The constructor for the Input Handler. This class
     * handles the command line input to the server this
     * mainly consists of passing data to the server or
     * extracting it from the server.
     *
     * @param server The server class that needs to be sent the input.
     */
    public InputHandler(Server server) {
        this.server = server;
    }

    /**
     * This will be ran whenever a input scanner thread is started.
     * This will loop where it catches the the input from the
     * standard input and pass it on to the server or handle it
     * locally where that is possible (e.g. printing the help
     * function).
     */
    public void run() {
        Scanner sc = new Scanner(System.in);
        print("Enter help for a list of commands\n");
        for (prompt(); sc.hasNextLine(); prompt()) {

            String line = sc.nextLine().replaceAll("\n", "");
            if (line.length() == 0)
                continue;
            String[] args = line.split("%");
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("exit"))
                    System.exit(0);
                else
                    printHelp();


            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("export") && args[1].equalsIgnoreCase("users")) {
                    for (DataCoder.Users.User u : server.userData.getUsersList()) {
                        String l = "-------------------------";
                        print(u.getName() + " " + l.substring(0, 25-u.getName().length()) + " " + u.getAddress() + "\n");
                    }
                } else if (args[0].equalsIgnoreCase("export") && args[1].equalsIgnoreCase("polls")) {
                    for (DataCoder.Polls.Poll p : server.pollData.getPollsList()) {
                        print("Poll: " + p.getId() + "\n");
                        for (DataCoder.Polls.Poll.Vote v : p.getVotesList()) {
                            ArrayList<String> counter = new ArrayList<String>();
                            for (String token : v.getTokenList()) {
                                if (!counter.contains(token)) {
                                    counter.add(token);
                                }

                            }
                            String l = "-------------------------";
                            print("\t" + v.getCandidate() + " " + l.substring(0, 25-v.getCandidate().length()) + " " + counter.size()+ "\n");
                        }
                    }
                } else if (args[0].equalsIgnoreCase("addPoll")) {
                    try {
                        String location = "";
                        DataCoder.Polls.Poll.Builder poll = DataCoder.Polls.Poll.newBuilder();
                        FileInputStream fStream = new FileInputStream(args[1]);
                        DataInputStream in = new DataInputStream(fStream);
                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
                        String strLine;
                        while ((strLine = br.readLine()) != null) {
                            String[] handles = strLine.split("%");
                            if (handles.length == 4) {
                                location = handles[1];
                                poll.setId(handles[0]);
                                poll.setStartTime(handles[2]);
                                poll.setEndTime(handles[3]);
                                poll.setLocation(handles[1]);

                            } else if (handles.length == 2) {
                                boolean ext = false;
                                for (DataCoder.Polls.Poll.Candidate ca : poll.getCandidatesList()) {
                                    if (ca.getName().equalsIgnoreCase(args[0])) {
                                        ext = true;
                                    }
                                }
                                if (!ext) {
                                    DataCoder.Polls.Poll.Candidate.Builder c = DataCoder.Polls.Poll.Candidate.newBuilder();
                                    c.setName(handles[0]);
                                    c.setName(handles[1]);
                                    poll.addCandidates(c.build());
                                    DataCoder.Polls.Poll.Vote.Builder v = DataCoder.Polls.Poll.Vote.newBuilder();
                                    v.setCandidate(handles[0]);
                                    poll.addVotes(v.build());
                                }
                            }
                        }
                        for (DataCoder.Users.User u : server.userData.getUsersList()) {
                            PrintWriter tStream = new PrintWriter(new BufferedWriter(new FileWriter(poll.getId() + "_" + poll.getStartTime() + "_tokens.txt", true)));
                            if (u.getLocation().equalsIgnoreCase(location)) {
                                DataCoder.Polls.Poll.Token.Builder t = DataCoder.Polls.Poll.Token.newBuilder();
                                String tok = generateToken();
                                byte[] bytes = tok.getBytes("UTF-8");
                                MessageDigest md = MessageDigest.getInstance("MD5");
                                byte[] digest = md.digest(bytes);
                                t.setKey(Arrays.toString(digest));
                                tStream.write(tok + "\n");
                                tStream.flush();
                                poll.addTokens(t.build());
                            }

                        }
                        server.pollData.addPolls(poll.build());
                        try {
                            FileOutputStream fo = new FileOutputStream("poll.vd");
                            server.pollData.build().writeTo(fo);
                            fo.flush();
                            fo.close();
                        } catch (IOException e) {
                            print("Not able to write to the poll file.");
                        }
                        in.close();
                    } catch (Exception e) {
                        print("File does not exist\n");
                    }
                } else if (args[0].equalsIgnoreCase("addUser")) {
                    FileInputStream fStream = null;
                    try {
                        fStream = new FileInputStream(args[1]);

                        DataInputStream in = new DataInputStream(fStream);
                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
                        String strLine;
                        while ((strLine = br.readLine()) != null) {
                            String[] handles = strLine.split("%");
                            if (handles.length == 3) {
                                DataCoder.Users.User.Builder user = DataCoder.Users.User.newBuilder();
                                user.setName(handles[0]);
                                user.setAddress(handles[1]);
                                user.setLocation(handles[2]);
                                server.userData.addUsers(user.build());
                            }
                        }
                        try {
                            FileOutputStream fo = new FileOutputStream("user.vd");
                            server.userData.build().writeTo(fo);
                            fo.flush();
                            fo.close();
                        } catch (IOException e) {
                            print("Not able to write to the user file.");
                        }
                    }  catch (IOException e) {
                        print("The supplied file was not found.\n");
                    }
                }else
                    printHelp();
            } else if (args.length == 4) {
                if (args[0].equalsIgnoreCase("addUser")) {
                    server.userData.addUsers(newUser(args[1], args[2], args[3]));
                    try {
                        FileOutputStream fo = new FileOutputStream("user.vd");
                        server.userData.build().writeTo(fo);
                        fo.flush();
                        fo.close();
                    } catch (IOException e) {
                        print("Not able to write to the users file.");
                    }
                }
                else
                    printHelp();
            } else if (args.length == 5) {
                if (args[0].equalsIgnoreCase("addPoll")) {
                    server.pollData.addPolls(newPoll(args[1], args[2], args[3], args[4]));
                    try {
                        FileOutputStream fo = new FileOutputStream("poll.vd");
                        server.pollData.build().writeTo(fo);
                        fo.flush();
                        fo.close();
                    } catch (IOException e) {
                        print("Not able to write to the users file.");
                    }
                }
                else
                    printHelp();
            }
            else
                printHelp();
        }

    }

    public void runTestString(String testString){
        String[] args = testString.split("%");
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("exit"))
                System.exit(0);
            else
                printHelp();


        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("export") && args[1].equalsIgnoreCase("users")) {
                for (DataCoder.Users.User u : server.userData.getUsersList()) {
                    String l = "-------------------------";
                    print(u.getName() + " " + l.substring(0, 25-u.getName().length()) + " " + u.getAddress() + "\n");
                }
            } else if (args[0].equalsIgnoreCase("export") && args[1].equalsIgnoreCase("polls")) {
                for (DataCoder.Polls.Poll p : server.pollData.getPollsList()) {
                    print("Poll: " + p.getId() + "\n");
                    for (DataCoder.Polls.Poll.Vote v : p.getVotesList()) {
                        ArrayList<String> counter = new ArrayList<String>();
                        for (String token : v.getTokenList()) {
                            if (!counter.contains(token)) {
                                counter.add(token);
                            }

                        }
                        String l = "-------------------------";
                        print("\t" + v.getCandidate() + " " + l.substring(0, 25-v.getCandidate().length()) + " " + counter.size()+ "\n");
                    }
                }
            } else if (args[0].equalsIgnoreCase("addPoll")) {
                try {
                    String location = "";
                    DataCoder.Polls.Poll.Builder poll = DataCoder.Polls.Poll.newBuilder();
                    FileInputStream fStream = new FileInputStream(args[1]);
                    DataInputStream in = new DataInputStream(fStream);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        String[] handles = strLine.split("%");
                        if (handles.length == 4) {
                            location = handles[1];
                            poll.setId(handles[0]);
                            poll.setStartTime(handles[2]);
                            poll.setEndTime(handles[3]);
                            poll.setLocation(handles[1]);

                        } else if (handles.length == 2) {
                            boolean ext = false;
                            for (DataCoder.Polls.Poll.Candidate ca : poll.getCandidatesList()) {
                                if (ca.getName().equalsIgnoreCase(args[0])) {
                                    ext = true;
                                }
                            }
                            if (!ext) {
                                DataCoder.Polls.Poll.Candidate.Builder c = DataCoder.Polls.Poll.Candidate.newBuilder();
                                c.setName(handles[0]);
                                c.setName(handles[1]);
                                poll.addCandidates(c.build());
                                DataCoder.Polls.Poll.Vote.Builder v = DataCoder.Polls.Poll.Vote.newBuilder();
                                v.setCandidate(handles[0]);
                                poll.addVotes(v.build());
                            }
                        }
                    }
                    for (DataCoder.Users.User u : server.userData.getUsersList()) {
                        PrintWriter tStream = new PrintWriter(new BufferedWriter(new FileWriter(poll.getId() + "_" + poll.getStartTime() + "_tokens.txt", true)));
                        if (u.getLocation().equalsIgnoreCase(location)) {
                            DataCoder.Polls.Poll.Token.Builder t = DataCoder.Polls.Poll.Token.newBuilder();
                            String tok = generateToken();
                            byte[] bytes = tok.getBytes("UTF-8");
                            MessageDigest md = MessageDigest.getInstance("MD5");
                            byte[] digest = md.digest(bytes);
                            t.setKey(Arrays.toString(digest));
                            tStream.write(tok + "\n");
                            tStream.flush();
                            poll.addTokens(t.build());
                        }

                    }
                    server.pollData.addPolls(poll.build());
                    try {
                        FileOutputStream fo = new FileOutputStream("poll.vd");
                        server.pollData.build().writeTo(fo);
                        fo.flush();
                        fo.close();
                    } catch (IOException e) {
                        print("Not able to write to the poll file.");
                    }
                    in.close();
                } catch (Exception e) {
                    print("File does not exist\n");
                }
            } else if (args[0].equalsIgnoreCase("addUser")) {
                FileInputStream fStream = null;
                try {
                    fStream = new FileInputStream(args[1]);

                    DataInputStream in = new DataInputStream(fStream);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        String[] handles = strLine.split("%");
                        if (handles.length == 3) {
                            DataCoder.Users.User.Builder user = DataCoder.Users.User.newBuilder();
                            user.setName(handles[0]);
                            user.setAddress(handles[1]);
                            user.setLocation(handles[2]);
                            server.userData.addUsers(user.build());
                        }
                    }
                    try {
                        FileOutputStream fo = new FileOutputStream("user.vd");
                        server.userData.build().writeTo(fo);
                        fo.flush();
                        fo.close();
                    } catch (IOException e) {
                        print("Not able to write to the user file.");
                    }
                }  catch (IOException e) {
                    print("The supplied file was not found.\n");
                }
            }else
                printHelp();
        } else if (args.length == 4) {
            if (args[0].equalsIgnoreCase("addUser")) {
                server.userData.addUsers(newUser(args[1], args[2], args[3]));
                try {
                    FileOutputStream fo = new FileOutputStream("user.vd");
                    server.userData.build().writeTo(fo);
                    fo.flush();
                    fo.close();
                } catch (IOException e) {
                    print("Not able to write to the users file.");
                }
            }
            else
                printHelp();
        } else if (args.length == 5) {
            if (args[0].equalsIgnoreCase("addPoll")) {
                server.pollData.addPolls(newPoll(args[1], args[2], args[3], args[4]));
                try {
                    FileOutputStream fo = new FileOutputStream("poll.vd");
                    server.pollData.build().writeTo(fo);
                    fo.flush();
                    fo.close();
                } catch (IOException e) {
                    print("Not able to write to the users file.");
                }
            }
            else
                printHelp();
        }
        else
            printHelp();
    }


    private DataCoder.Users.User newUser(String name, String address, String location) {
        DataCoder.Users.User.Builder ub = DataCoder.Users.User.newBuilder();
        ub.setName(name);
        ub.setAddress(address);
        ub.setLocation(location);
        return ub.build();
    }

    private void printHelp(){
        print("The system accepts the following commands separated by %\n");
        print("export [polls, users]\n");
        print("\tExport the user data or the poll data to standard output\n");
        print("addPoll (id, location, start date, end date)\n");
        print("\tAdds a poll with the specified parameters to the system.\n");
        print("addPoll (filepath)\n");
        print("\tAdds a poll from the specified file.\n");
        print("addUser (filepath)\n");
        print("\tAdds a list of users from the specified file.\n");
        print("addUser (name, address, vote location)\n");
        print("\tAdds user with the specified parameters to the system.\n");
    }

    private DataCoder.Polls.Poll newPoll(String id, String location, String start, String end) {
        DataCoder.Polls.Poll.Builder poll = DataCoder.Polls.Poll.newBuilder();
        String next = "";
        Scanner sc1 = new Scanner(System.in);
        poll.setId(id);
        poll.setStartTime(start);
        poll.setEndTime(end);
        poll.setLocation(location);
        print("Enter candidates name and address separated by % then return to submit, when all candidates added type done to resume\n");
        while (!next.equalsIgnoreCase("done")) {
            print(">>> ");
            String line = sc1.nextLine().replaceAll("\n", "");
            if (line.length() == 0)
                continue;
            String[] args = line.split("%");
            if (args.length == 2) {
                boolean ext = false;
                for (DataCoder.Polls.Poll.Candidate ca : poll.getCandidatesList()) {
                    if (ca.getName().equalsIgnoreCase(args[0])) {
                        ext = true;
                    }
                }
                if (!ext) {
                    DataCoder.Polls.Poll.Candidate.Builder c = DataCoder.Polls.Poll.Candidate.newBuilder();
                    c.setName(args[0]);
                    c.setName(args[1]);
                    poll.addCandidates(c.build());
                    DataCoder.Polls.Poll.Vote.Builder v = DataCoder.Polls.Poll.Vote.newBuilder();
                    v.setCandidate(args[0]);
                    poll.addVotes(v.build());
                }
            } else {
                break;
            }
        }
        for (DataCoder.Users.User u : server.userData.getUsersList()) {
            if (u.getLocation().equalsIgnoreCase(location)) {
                DataCoder.Polls.Poll.Token.Builder t = DataCoder.Polls.Poll.Token.newBuilder();
                t.setKey(generateToken());
                poll.addTokens(t.build());
            }
        }
        return poll.build();
    }

    private void prompt() {
        print(">> ");
    }

    private void print(String s) {
        System.out.print(s);
    }

    private String generateToken() {
        long timeSeed = System.nanoTime();
        double randSeed = Math.random() * 1000;
        long midSeed = (long) (timeSeed * randSeed);
        String s = midSeed + "";
        return s.substring(0, 9);
    }
}
