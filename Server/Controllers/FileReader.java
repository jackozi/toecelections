package Server.Controllers;

import Res.DataCoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 1/31/14
 * Time: 4:05 PM
 */
public class FileReader{
    private Server server;
    /**
     * Constructor for the file reader. This connects it to the server
     * it passes its data to.
     * @param s The server that receives data from this reader.
     */
    public FileReader(Server s){
        this.server = s;
    }

    /**
     * This makes the fileReader read a file from disk
     * this method is called whenever the server receives a file
     * from the client. Normally this would be achieved using a
     * watch service on the data directory but as this is rather
     * complicated and out of scope for the objective of this system
     * we cheated here by doing it this way.
     * @param file The file containing poll data
     */
    public void readFile(String file){
        int temp = 0;
        boolean success = false;

        DataCoder.Polls.Poll.Builder poll = DataCoder.Polls.Poll.newBuilder();
        try {

            poll.mergeFrom(new FileInputStream(file));
            success = new File(file).delete();

        } catch (IOException e) {
            System.out.print("\nThe file reader choked in a file. This might void vote data.\n");
            if(!success)
                System.out.print("The file however, is still present on the system.");
            System.out.print("\n>> ");
        }
        for(DataCoder.Polls.Poll p : server.pollData.getPollsList()){
            if(p.getId().equalsIgnoreCase(poll.getId()) && p.getStartTime().equalsIgnoreCase(poll.getStartTime())){
                for(DataCoder.Polls.Poll.Vote vote : p.getVotesList()){
                    int temp1 = 0;
                    for(DataCoder.Polls.Poll.Vote v: poll.getVotesList() ){
                        if(v.getCandidate().equals(vote.getCandidate())){
                            DataCoder.Polls.Poll.Vote.Builder vm = v.toBuilder();
                            for(String token : vote.getTokenList()){
                                vm.addToken(token);
                            }
                            poll.setVotes(temp1, vm.build());
                        }
                        temp1++;
                    }

                }
                server.pollData.setPolls(temp, poll);
                try {
                    FileOutputStream output = new FileOutputStream("poll.vd");

                    server.pollData.build().writeTo(output);

                    output.close();
                } catch (IOException e) {
                    System.out.print("Failed to update the local data files. This might void vote data.\n");
                    if(!success)
                        System.out.print("The file however, is still present on the system.");
                    System.out.print("\n>> ");
                }
            }
            temp++;
        }
        }
    }
