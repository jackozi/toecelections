package Server.Controllers;

import Res.DataCoder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 1/19/14
 * Time: 2:13 PM
 */
public class Server extends Thread{
    private int port;
    FileReader fh;
    InputHandler ih;
    protected DataCoder.Polls.Builder pollData = DataCoder.Polls.newBuilder();
    protected DataCoder.Users.Builder userData = DataCoder.Users.newBuilder();

    /**
     * The main class for the server. It initializes
     * a new server class and then starts a thread
     * running it.
     * @param args The arguments getting passed along
     *             when starting the programm. These
     *             are useless at this moment.
     */
    public static void main(String[] args){
        Server s = new Server();
        s.start();

    }

    public void runTestString(String testString){
        ih.runTestString(testString);
    }

    /**
     * Constructor for the server class.This initializes some
     * settings and then starts an input handler and reads the
     * poll data available in files poll.vd and user.vs in the
     * directory it is ran. If these files are not present it
     * creates them. These files are also used to store data
     * during runtime so the chances of data loss during crash
     * are minimized
     */
    public Server(){
        port = 3301;
        ih = new InputHandler(this);
        fh = new FileReader(this);
        ih.start();
        readData();
    }

    /**
     * Passes through the file to be read to the file reader.
     * @param file the file to be sent to the file reader
     */
    public void readFile(String file){
        fh.readFile(file);
    }

    /**
     * This method is executed when a server thread is started
     * it waits for a client to connect and then starts a connection
     * with this client.
     */
    public void run(){
        ServerSocket server;

    try {
        server = new ServerSocket(port);
        //noinspection InfiniteLoopStatement
        while(true) {
            Socket socket = server.accept();
            Connection client = new Connection(socket,
                    this);
            client.start();
        }
    } catch (IOException e) {
        print("\nThe server could not be initialised data can however still be retrieved. To accept connections please restart the server.\n>> ");
    }
    }

    /**
     * Takes a command from the connection to a client and
     * handles it. This will mostly be a request for data
     * in this case the poll data is passed to the client
     * through the connection utilizing the Connection.sendFile
     * method.
     * @param read The command to be handled by the server
     * @param connection The connection that sent this command
     */
    public void executeCommand(String read, Connection connection) {
        String[] command = read.split("%");

        if(command[0].equals("pollData")){
            try {
                DataCoder.Polls polls = DataCoder.Polls.parseFrom(new FileInputStream("poll.vd"));
                for(DataCoder.Polls.Poll p : polls.getPollsList()){
                    if(p.getLocation().equals(command[1])){
                        connection.sendFile(p);
                    }
                }
            } catch (IOException e) {
                print("The datafiles have been tampered with during runtime, please restart the server.");
                System.exit(11);
            }
        }
    }

    private void print(String x){
        System.out.print(x);
    }

    private void readData(){
        try {
            try {
                pollData.mergeFrom(new FileInputStream("poll.vd"));
            } catch (FileNotFoundException e) {
                System.out.println("poll.vd: File not found.  Creating a new file.");
                FileOutputStream output = new FileOutputStream("poll.vd");
                pollData.build().writeTo(output);
                output.close();
            }
            try {
                userData.mergeFrom(new FileInputStream("user.vd"));
            } catch (FileNotFoundException e) {
                System.out.println("user.vd: File not found.  Creating a new file.");
                FileOutputStream output = new FileOutputStream("user.vd");
                userData.build().writeTo(output);
                output.close();
            }
        }catch (IOException e) {
            print("Either the \"poll.vd\" or \"user.vd\" datafile has been tampered with. Please destroy the corrupted file or restore an backup");
        }

    }
}


