package Tests;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 2/1/14
 * Time: 3:32 AM
 */

import Server.Controllers.Server;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests for {@link Server.Controllers.Server}.
 * @author Jacko Zuidema
 */
@RunWith(JUnit4.class)
public class TestServer {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    Server s;
    @Before
    public void setUpStreams() {
        new File("user.vd").delete();
        System.setOut(new PrintStream(outContent));
        s = new Server();
        s.start();
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void TestAdding() throws Exception {
        outContent.reset();
        s.runTestString("help");
        assertEquals("The system accepts the following commands separated by %\n" +
                "export [polls, users]\n" +
                "\tExport the user data or the poll data to standard output\n" +
                "addPoll (id, location, start date, end date)\n" +
                "\tAdds a poll with the specified parameters to the system.\n" +
                "addPoll (filepath)\n" +
                "\tAdds a poll from the specified file.\n" +
                "addUser (filepath)\n" +
                "\tAdds a list of users from the specified file.\n" +
                "addUser (name, address, vote location)\n" +
                "\tAdds user with the specified parameters to the system.\n", outContent.toString());
        outContent.reset();
        s.runTestString("export%users");
        assertEquals("", outContent.toString());
        outContent.reset();
        s.runTestString("adduser%jacko%thuis%Enschede");
        assertEquals("", outContent.toString());
        outContent.reset();
        s.runTestString("adduser%jacko%thuis%Enschede%nee");
        assertEquals("The system accepts the following commands separated by %\n" +
                "export [polls, users]\n" +
                "\tExport the user data or the poll data to standard output\n" +
                "addPoll (id, location, start date, end date)\n" +
                "\tAdds a poll with the specified parameters to the system.\n" +
                "addPoll (filepath)\n" +
                "\tAdds a poll from the specified file.\n" +
                "addUser (filepath)\n" +
                "\tAdds a list of users from the specified file.\n" +
                "addUser (name, address, vote location)\n" +
                "\tAdds user with the specified parameters to the system.\n", outContent.toString());
        outContent.reset();
        s.runTestString("adduser%jacko%thuis%Enschede");
        assertEquals("", outContent.toString());
        outContent.reset();
        s.runTestString("export%users");
        assertEquals("jacko -------------------- thuis\n" +
                "jacko -------------------- thuis\n", outContent.toString());
    }

}