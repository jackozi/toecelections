package Tests;

/**
 * Created with IntelliJ IDEA.
 * User: Rob
 * Date: 2/1/14
 * Time: 3:32 AM
 */

import Res.DataCoder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import Client.Controllers.*;
import Server.Controllers.*;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import static junit.framework.Assert.assertEquals;

/**
 * Tests for {@link Client.Controllers.Client}.
 * @author Rob Stortelder
 */
@RunWith(JUnit4.class)
public class TestClient {


    @Before @After
    public void removeFiles(){
        new File("user.vd").delete();
        new File("poll.vd").delete();
        new File("poll_c.vd").delete();
        new File("Waterschapsverkiezing_1-2-2014_tokens.txt").delete();
    }


    @Test
    /**
     * This tests most elements from the clTestClient {
ient
     * @require the provided original test files poll.txt and users.txt
     */
    public void testClient() {
        //Remove all the files if they are available on start of the test
        String token = "";

        //Test connection
        Client c = new Client(true);
        assertEquals("Polldata empty on start test", true, c.getPoll().getCandidatesList().size()<1);
        assertEquals("Check not existing server with correct port", false, c.checkServer("Nergens", 3301, "Enschede") );
        assertEquals("check not existing server with wrong port",false, c.checkServer( "localhost", 2502, "Eindhoven"));
        Server s = new Server();
        s.start();
        s.runTestString("adduser%TestData/Users.txt");
        s.runTestString("addpoll%TestData/Poll.txt");
        assertEquals("Working server, wrong port", false, c.checkServer("localhost", 3302, "Enschede"));
        assertEquals("working server", true, c.checkServer("localhost", 3301, "Enschede"));
        assertEquals("PollData available after connecting to the server", true, c.getPoll().getCandidatesList().size()>0);
        Client c2 = new Client(true);
        assertEquals("Second client recognized available polldata from other client", true, c2.getPoll().getCandidatesList().size()>0);

        //Test remainingdays function
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String currentDate = dateFormat.format(cal.getTime());
        c.getPoll().setStartTime(dateFormat.format(cal.getTime()));
        assertEquals("remaining days with current date", 0, c.getRemainingDays());

        String[] today = currentDate.split("-");
        int next = Integer.valueOf(today[2])+1;
        int previous = Integer.valueOf(today[2])-1;
        today[2] = String.valueOf(next);
        String futureDate = today[0] +"-"+ today[1] +"-"+today[2];
        today[2] = String.valueOf(previous);
        String pastDate = today[0] +"-"+ today[1] +"-"+today[2];

        c.getPoll().setStartTime(futureDate)           ;
        assertEquals("Test remaining days with future date", true, c.getRemainingDays()>0);
        c.getPoll().setStartTime(pastDate);
        assertEquals("Test remaining days with past date", true, c.getRemainingDays()<0);

        // check token
        try{
            RandomAccessFile inFile = new RandomAccessFile("Waterschapsverkiezing_1-2-2014_tokens.txt","rw");
            token = inFile.readLine();
            inFile.close();
        }catch(IOException e){
            System.out.println("Problem while reading token file");
        }
        assertEquals("Check token first time", true, c.verifyToken(token));
        assertEquals("Check token second time", false, c.verifyToken(token));

        c.vote("Geert Wilders", token);
        assertEquals("Vote for the specific candidate", 1 , c.getPoll().getVotesList().get(8).getTokenCount());

        //send the data to the server and remove the file
        c.sendData();
        assertEquals("Check if sending the data works", false, new File("poll_c.vd").exists());

    }
}