package Client.Controllers;

import Client.Views.TokenView;
import Client.Views.VoteView;
import Res.DataCoder;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;


/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 1/19/14
 * Time: 2:13 PM
 */
public class Client extends Thread implements KeyListener {
    private TokenView tokenView;
    private VoteView voteView;
    private Socket socket;
    private BufferedWriter out;
    private DataCoder.Polls.Poll.Builder pollData = DataCoder.Polls.Poll.newBuilder();
    private boolean disableVisual;


    /**
     * Constructor of the client. This creates and enables te TokenView.
     * ReadData is called to read data stored locally in case of a client crash or forced shutdown
     * @param disableVisual is used for testing purposes, so no JFrame is started
     */
    public Client(Boolean disableVisual) {
        this.disableVisual = disableVisual;
        readData();
        if(!disableVisual){
            tokenView = new TokenView(this);
            enableTokenView();
        }
    }

    /**
     * This function reads data stored locally. This can only happen if the client crashed or is closed the wrong way and could not send the file to the server.
     * If there is no data stored locally it creates an empty poll_c.vd file and builds is from the polData.
     * If this function is unable to create a file locally, a message dialog will be shown.
     */
    private void readData() {
        try {
            try {
                pollData.mergeFrom(new FileInputStream("poll_c.vd"));
            } catch (FileNotFoundException e) {
                FileOutputStream output = new FileOutputStream("poll_c.vd");
                pollData.build().writeTo(output);
                output.close();
            }
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(tokenView, "Unable to create a local data file");
        }
    }

    /**
     * @return the poll data from the builder
     */
    public DataCoder.Polls.Poll.Builder getPoll() {
        return pollData;
    }

    /**
     * Disables the VoteView.
     */
    public void disableVoteView() {
        voteView.setEnabled(false);
        voteView.setVisible(false);
    }

    /**
     * Disables the TokenView and makes it unvisible
     */
    public void disableTokenView() {
        tokenView.setEnabled(false);
        tokenView.setVisible(false);
    }

    /**
     * Enables the Voteview and makes is visible.
     * Regenerates the voteView for every specific token, this is used if there are several polls at the same time
     *
     * @param token the token entered in the tokenView
     */
    public void enableVoteView(String token) {
        voteView = new VoteView(this, token);
        voteView.addKeyListener(this);
        voteView.setEnabled(true);
        voteView.setVisible(true);
    }

    /**
     * Enables the TokenView and makes it visible
     */
    public void enableTokenView() {
        tokenView.setEnabled(true);
        tokenView.setVisible(true);
    }

    /**
     * resets the tokenView for the next user
     */
    public void resetTokenView() {
        tokenView.clearToken();
    }

    /**
     * Checks te server credentials and set up an outputstream. After succesfully setting up the stream,
     * After setting up the stream the candidatelist is pulled if there is no candidatelist available from a previous crash or forced shutdown.
     * @param hostname the host name of the server
     * @param port     the port for the server
     * @param location the location of the polling station
     * @return true if the credentials are correct, false if they are incorrect
     */
    public boolean checkServer(String hostname, int port, String location) {
        try {
            socket = new Socket(hostname, port);
            out = new BufferedWriter(new OutputStreamWriter(
                    socket.getOutputStream()));
            out.write("pollData%" + location + "\n");
            out.flush();

        } catch (IOException e) {
            return false;
        }
        if (pollData.getCandidatesList().size() < 1) {
                try {
                    pollData.mergeDelimitedFrom(socket.getInputStream());
                    FileOutputStream output;
                    try {
                        output = new FileOutputStream("poll_c.vd");
                    } catch (FileNotFoundException e) {
                        return false;
                    }
                    pollData.build().writeTo(output);
                    output.close();
                } catch (IOException e) {
                    return false;
                }
            }
        return true;
    }

    /**
     * Verifies a specific token with the tokenList in the poll data.
     * If the token is correct, the token will be removed from the list and true will be returned
     * @param token the entered token to check with the tokenlist
     * @return true if the token is correct, falce if the token is incorrect
     */
    public boolean verifyToken(String token) {
        try {
        byte[] bytes = token.getBytes("UTF-8");
        MessageDigest md = null;

            md = MessageDigest.getInstance("MD5");

        byte[] digest = md.digest(bytes);
        int temp = 0;
        for (DataCoder.Polls.Poll.Token t : pollData.getTokensList()) {
            if (t.getKey().equals(Arrays.toString(digest))){
                pollData.removeTokens(temp);
                return true;
            }
            temp++;

        }
        } catch (NoSuchAlgorithmException e) {
            JOptionPane.showMessageDialog(tokenView, "Unsupported operating system detected, please upgrade your system");
        } catch (UnsupportedEncodingException e) {
            JOptionPane.showMessageDialog(tokenView, "Unsupported operating system detected, please upgrade your system");
        }
        return false;
    }


    /**
     * If the format is correct, this method will return the remaining days for the poll
     * Positive remaining days are in the future, negative in the past
     * If the format of the start time of the poll is incorrect this will return -1.
     */

    public int getRemainingDays(){

        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
        String election = pollData.getStartTime();

        try {
            Date date1 = myFormat.parse(election);
            Date date2 = new Date();
            date2.setHours(0);
            long diff = date1.getTime() - date2.getTime();
            return ((int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            return -1;
        }

    }

    /**
     * Adds one vot to a specific candidate. After adding the token, the voteView is disabled and the tokenView enabled
     * @param candidateName the name of the candidate
     * @param token the token entered in the tokenView
     */
    public void vote(String candidateName, String token){
        int temp = 0;
        for(DataCoder.Polls.Poll.Vote v: pollData.getVotesList() ){
            if(v.getCandidate().equals(candidateName)){
                DataCoder.Polls.Poll.Vote.Builder vm = v.toBuilder();
                vm.addToken(token);
                pollData.setVotes(temp, vm.build());
            }
            temp++;
        }
        if(!disableVisual){
            disableVoteView();
            resetTokenView();
            enableTokenView();
        }
    }


    /**
     * Listens to keys pressed from specific components.
     * If the key combination ctrl+q is sendData is called
     * @param e the keyevent triggering thie KeyListener
     */
    public void keyPressed(KeyEvent e) {
        if (e.isControlDown() && e.getKeyChar() != 'q' && e.getKeyCode() == 81) {

            if (out != null) {
               sendData();
            }
            System.exit(0);
        }
    }

    /**
     * This method is called when exiting the program.
     * This sends the voting data to the server
     */
    public void sendData(){
        if (out != null) {
            try {
                out.write("voteData\n");
                out.flush();
                pollData.build().writeDelimitedTo(socket.getOutputStream());
                new File("poll_c.vd").delete();

            } catch (IOException e1) {
               System.exit(1);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        //Method is not used
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        //Method is not used
    }

    /**
     * This is the main function that starts the program
     * @param args the arguments given on startup of the program
     */
    public static void main(String[] args) {
        new Client(false);
    }

}
