package Client.Views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Client.Controllers.Client;
import Res.DataCoder;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 1/19/14
 * Time: 2:14 PM
 */
public class VoteView extends JFrame implements ActionListener {
    /**
     * The variables used for this voteView.
     * client is the client used to generate this voteView.
     * Token is the token entered before starting this voteView
     */
    Client client;
    String token;

    /**
     * Constructor, This generates the panel where votes are entered.
     * This panel is full screen and cannot be closed
     * @param c the client used to enable this view
     * @param token the token entered in the tokenView
     */
    public VoteView(Client c, String token) {
        super("Enter token");
        this.token = token;
        client = c;
        createVotePanel();
        this.setUndecorated(true);
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
        this.setVisible(true);
    }

    /**
     * Creates the panel with a button for each candidate.
     * The candidates are received from the client using the client.getPoll() function.
     * The buttons are equally divided over the screen
     */
    private void createVotePanel() {
        int size = (int) Math.ceil(Math.sqrt(client.getPoll().getVotesList().size()));
        this.setLayout(new GridLayout(size, size));
        for (DataCoder.Polls.Poll.Vote v : client.getPoll().getVotesList()) {
            JButton b = new JButton(v.getCandidate());
            b.setName(v.getCandidate());
            b.addActionListener(this);
            this.add(b);
        }

    }

    /**
     * This is executed everytime a jButton is pressed
     * the name of the candidate is pulled from the button.
     * With client.getPoll the candidate is searched in the file and the token is added to the candidate
     * After the vote is added, this view is disabled and the view for entering the token is resetted and enabled.
     * @param e the source of the actionEvent
     */
    public void actionPerformed(ActionEvent e) {
        client.vote(((JButton) e.getSource()).getName(), token);

    }



}
