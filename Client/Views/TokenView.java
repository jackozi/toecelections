package Client.Views;

import Client.Controllers.Client;
import javax.swing.*;
import java.awt.*;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 1/19/14
 * Time: 2:12 PM
 */
public class TokenView extends JFrame implements ActionListener {

    private JTextField tokenFld;
    private JLabel infoLabel;
    private JPanel keyboard;
    private JButton backspace, submit;
    private Client client;

    /**
     * Set the standard settings for the main panel. This panel is displayed fullscreen with an grid layout of 3,1
     * To close this screen the key combination Ctrl+q can be pressed. The data will be send to the server and the program will shut down
     * When first displayed a serverdialog is created. This is used to enter the server credentials.
     * @param c the client used to start this view
     */
    public TokenView(Client c) {
        super("enter token");
        client = c;
        this.setLayout(new GridLayout(3, 1));
        this.setUndecorated(true);
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
        initializeElements();
        addElements();
        this.setVisible(true);
        new ServerDialog(this, client);
    }

    /**
     * Initialize all the components and configure them correctly. This will also set the font sizes for the components right.
     * At each element an keyListener will be added. If the focus is on that element, the keyPressed() function will be called every time a key is pressed.
     */
    private void initializeElements() {
        infoLabel = new JLabel("Please enter your token and touch the submit button.");
        Font labelFont = new Font(infoLabel.getFont().getName(), infoLabel.getFont().getStyle(), (int) (0.04 * Toolkit.getDefaultToolkit().getScreenSize().getHeight()));
        infoLabel.setVerticalTextPosition((int) CENTER_ALIGNMENT);
        infoLabel.setFont(labelFont);
        submit = new JButton("Submit");
        backspace = new JButton("backspace");
        tokenFld = new JTextField("Enter token");
        tokenFld.addKeyListener(client);
        Font tokenFont = new Font(tokenFld.getFont().getName(), tokenFld.getFont().getStyle(), (int) (0.3 * Toolkit.getDefaultToolkit().getScreenSize().getHeight()));
        tokenFld.setFont(tokenFont);
        keyboard = new JPanel();
        createKeyboardPanel();
        tokenFld.addKeyListener(client);
        submit.addKeyListener(client);
        backspace.addKeyListener(client);
    }

    /**
     * Add all the elements to the JFrame.
     * It is important to keep the order right, because the gridLayout will order the elements correctly
     */
    private void addElements() {
        this.add(infoLabel);
        this.add(tokenFld);
        this.add(keyboard);
    }

    /**
     * This creates the keyboard with the backspace and the submit buttons to the right locations.
     * This also adds actionListeners and keyListeners to all the buttons for listening to clicking and the Ctrl+q combination
     */
    private void createKeyboardPanel() {
        keyboard.setLayout(new GridLayout(2, 6));
        keyboard.setVisible(true);
        for (int i = 0; i <= 9; i++) {
            if (i == 5) {
                keyboard.add(backspace);
                backspace.addActionListener(this);
                backspace.setName("b");
            }
            JButton b = new JButton(String.valueOf(i));
            b.setName(String.valueOf(i));
            b.addActionListener(this);
            b.addKeyListener(client);
            keyboard.add(b);
            b.setVisible(true);
        }
        keyboard.add(submit);
        submit.addActionListener(this);
        submit.setName("s");
    }

    /**
     * This are all the action listeners for the buttons
     *
     * @param e The action event of the button
     */
    public void actionPerformed(ActionEvent e) {
        /**
         * If the text in the tokenField does not only contain numbers, the text will be cleared.
         */
        if (!tokenFld.getText().matches("[0-9]+")) {
            tokenFld.setText("");
        }
        String input = ((JButton) e.getSource()).getName();
        /**
         * Check if the pushed button is backspace. if so, remove the last character from the textfield string.
         */
        if (input.equals("b")) {
            String text = tokenFld.getText();
            if (text.length() > 0) {
                tokenFld.setText(text.substring(0, text.length() - 1));
            }
        }
        /**
         * check if the pushed button is submit. if so, check the entered code and proceed to the next screen
         * If the code is wrong, change the message on top of the screen.
         */
        else if (input.equals("s")) {
            submit.setEnabled(false);
            String token = tokenFld.getText();
            if (client.verifyToken(tokenFld.getText())) {
                this.setVisible(false);
                submit.setEnabled(true);
                client.disableTokenView();
                client.enableVoteView(token);
            } else {
                tokenFld.setText("Invalid");
                submit.setEnabled(true);
            }
        }
        /**
         * This is the last check. The name of the button will always be an integer. This integer will be parsed and added
         * to the string of integers
         */
        else {
            tokenFld.setText(tokenFld.getText() + Integer.parseInt(input));
        }
    }

    /**
     * This function is called from the serverdialog.to check if the server credentials are right.
     * The server credentials will de send to the client to check and set up the connection
     * @param hostname The hostname of the server.
     * @param port The portnumber of the server
     * @param location The location of the poll client
     * @return true if the server is successfully checked, false is the credentials are wrong.
     */
    public boolean checkServer(String hostname, int port, String location) {
        return client.checkServer(hostname, port, location);
    }

    /**
     * Clears the tokenField. Replaces any text with the text "Enter token".
     */
    public void clearToken() {
        tokenFld.setText("Enter token");
    }

    public void setCountdown(){
        int daysremaining = client.getRemainingDays();
        if(daysremaining >0){
            JOptionPane.showMessageDialog(this, "Days until start next poll: " + daysremaining);
        }



    }

}
