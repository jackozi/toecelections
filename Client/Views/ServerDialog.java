package Client.Views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Client.Controllers.Client;

/**
 * Created with IntelliJ IDEA.
 * User: rob
 * Date: 1/30/14
 * Time: 2:05 PM
 */

public class ServerDialog extends JDialog implements ActionListener {
    private JPanel inputPanel;
    private TokenView tokenView;
    private JTextField addressField;
    private JTextField portField;
    private JTextField locationField;
    private JLabel infoLabel;
    private JButton submitBtn;
    private Client client;

    /**
     * Constructor for creating a serverDialog. This dialog has three fields for entering the server credentials.
     * @param parent the parent JPanel used for this view. In this case it is a tokenView.
     * @param c the client class used in this program. Used for setting the keyListeners to quit the program with ctrl+q
     */
    public ServerDialog(TokenView parent, Client c) {
        super(parent, "Enter server information", true);
        this.tokenView = parent;
        this.client = c;
        setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        createInputPanel();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        setSize((int) (toolkit.getScreenSize().getWidth() / 3), (int) (toolkit.getScreenSize().getHeight() / 3));
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        getContentPane().add(inputPanel);
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    /**
     * Creates the inputPanel for this dialog.And add them to the inputPanel.
     * This creates a label, for information, three fields for the server credentials and a button to submit the entered information
     * Actionlisteners an keylisteners are added to provide a shutdown of the program with ctrl+q and to listen to the submit button pressed
     */
    private void createInputPanel() {
        inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(5, 1));
        infoLabel = new JLabel("Enter server credentials");
        addressField = new JTextField("Enter Server Address");
        portField = new JTextField("3301");
        locationField = new JTextField("Enter Poll Location");
        submitBtn = new JButton("Submit");
        submitBtn.addActionListener(this);
        addressField.addKeyListener(client);
        locationField.addKeyListener(client);
        portField.addKeyListener(client);
        submitBtn.addKeyListener(client);
        inputPanel.add(infoLabel);
        inputPanel.add(addressField);
        inputPanel.add(portField);
        inputPanel.add(locationField);
        inputPanel.add(submitBtn);
    }


    /**
     * The action listener for the submit button. Pulls information from the fields and sends them to the checkserver from tokenview to be checked
     * If checkServer returns true, the dialog will be disposed and a message with the remaining days is displayed
     * @param e the actionEvent that triggers this listener.
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            submitBtn.setEnabled(false);
            try {
                if (tokenView.checkServer(addressField.getText(), Integer.parseInt(portField.getText()), locationField.getText())) {
                    dispose();
                    tokenView.setCountdown();
                } else {
                    infoLabel.setText("Server connection not possible");
                }
            } catch (NumberFormatException ex) {
                infoLabel.setText("Port is not an integer");
            }
            submitBtn.setEnabled(true);

        }
    }
}
